﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TileManager
{
    public static WorldConfig worldConfig;

    public static void Initialise( WorldConfig worldConfig )
    {
        TileManager.worldConfig = worldConfig;
    }

    public static void OnTileTypeChanged( Tile tile, GameObject tileGO )
    {
        MeshRenderer renderer = tileGO.GetComponentInChildren<MeshRenderer>();

        SetTileMaterial( tile.Type, renderer );
        SetTileHue( tile, renderer );
    }

    public static void OnHeightChanged( Tile tile, GameObject tileGO )
    {
        Vector3 tempPos = tileGO.transform.position;
        tempPos.y = tile.height;
        tileGO.transform.position = tempPos;
    }

    private static void SetTileMaterial( Terrain.Type type, MeshRenderer renderer )
    {
        renderer.material = worldConfig.terrains.Find( x => x.type == type ).material;
    }

    private static void SetTileHue( Tile tile, MeshRenderer renderer )
    {
        int typeIndex = worldConfig.terrains.FindIndex( x => x.type == tile.Type );

        List<Color> hues = GameAssets.Instance.terrainHues.Find( x => x.type == tile.Type ).hues;

        if ( hues == null )
            return;

        if ( hues.Count <= 0 )
            return;

        int colorIndex = 0;
        if ( typeIndex == 0 )
        {
            colorIndex = Mathf.FloorToInt( tile.height.Map(
                worldConfig.terrains[typeIndex].heightThreshold,
                0,
                0,
                hues.Count ) );
        }
        else if ( typeIndex < worldConfig.terrains.Count )
        {
            colorIndex = Mathf.FloorToInt( tile.height.Map(
                worldConfig.terrains[typeIndex].heightThreshold,
                worldConfig.terrains[typeIndex + 1].heightThreshold,
                0,
                hues.Count ) );
        }

        renderer.material.color = hues[colorIndex];
    }
}
