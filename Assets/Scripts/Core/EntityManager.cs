﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EntityManager
{
    public static void OnDeath( LivingEntity entity, GameObject obj )
    {
        Object.Destroy( obj );
    }
}
