﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WorldManager : MonoBehaviour
{
    // Settings
    [Header( "World Options" )]
    [SerializeField]
    private WorldConfig worldConfig;
    public WorldConfig WorldConfig { get { return worldConfig; } }

    public World world { private set; get; }

    public static WorldManager Instance;

    private void Awake()
    {
        if ( Instance != null )
            Destroy( gameObject );

        Instance = this;
        TileManager.Initialise( worldConfig );

        if ( world == null )
            world = WorldGenerator.GetWorld( worldConfig );

        CreateWorld();
    }

    private void CreateWorld()
    {
        GameObject worldParent = new GameObject( "_worldParent" );
        GameObject entityParent = new GameObject( "_plantParent" );

        for ( int x = 0; x < world.width; x++ )
        {
            for ( int y = 0; y < world.height; y++ )
            {
                Tile tileData = world.GetTileAt( x, y );

                CreateTileObject( tileData, worldParent.transform );

                if ( tileData.entity != null )
                    CreateEntityObject( tileData, entityParent.transform );
                    
            }
        }
    }

    private void CreateTileObject( Tile tileData, Transform parent )
    {
        float height = tileData.Type == Terrain.Type.Water ? worldConfig.terrains.Find( t => t.type == Terrain.Type.Water ).heightThreshold : tileData.height;
        height = worldConfig.isFlat ? 0 : height;

        GameObject tileGO = Instantiate(
            GameAssets.Instance.tilePrefab,
            new Vector3( tileData.x, height, tileData.y ),
            Quaternion.identity,
            parent );

        tileGO.name = $"{tileData.x} _ {tileData.y}";

        tileData.onTileTypeChanged += ( sender, tile ) => { TileManager.OnTileTypeChanged( tile, tileGO ); };
        tileData.onHeightChanged += ( sender, tile ) => { TileManager.OnHeightChanged( tile, tileGO ); };

        TileManager.OnTileTypeChanged( tileData, tileGO );

        tileData.SetHeight( height );
    }

    private void CreateEntityObject( Tile tileData, Transform parent )
    {
        GameObject obj = null;
        if ( tileData.entity.species == Species.Tree )
            obj = GameAssets.Instance.GetTreeOfType( tileData.Type );
        else if ( tileData.entity.species == Species.Plant )
            obj = GameAssets.Instance.GetPlantOfType( tileData.Type );

        if ( obj == null )
            return;

        GameObject entityGO = Instantiate(
            obj,
            new Vector3( tileData.x, tileData.height, tileData.y ),
            Quaternion.Euler( 0, Random.Range( 0f, 360f ), 0 ),
            parent );

        tileData.entity.onDeath += ( sender, entity ) => { EntityManager.OnDeath( entity, entityGO ); };
    }
}
