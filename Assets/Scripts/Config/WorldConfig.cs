﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

[CreateAssetMenu( fileName = "WorldConfig", menuName = "Configs/World/WorldConfig", order = 0 )]
public class WorldConfig : ScriptableObject
{
    public int width = 100;
    public int height = 100;

    [Header( "Height Settings" )]
    public bool isFlat = false;
    public float minHeight = 0;
    public float maxHeight = 100;

    [Space( 10 )]
    public List<HeightModifier> heightModifiers = new List<HeightModifier>()
    {
        new HeightModifier()
    };

    [Header( "Terrain Settings" )]
    public List<Terrain> terrains = new List<Terrain>()
    {
        new Terrain { name = "Water", type = Terrain.Type.Water },
        new Terrain { name = "Sand", type = Terrain.Type.Sand },
        new Terrain { name = "Grass", type = Terrain.Type.Grass },
        new Terrain { name = "Stone", type = Terrain.Type.Stone },
        new Terrain { name = "Snow", type = Terrain.Type.Snow },
    };

    [Header( "Plant Settings" )]
    public float treeProbability = .1f;
    public float plantProbability = .1f;
}

