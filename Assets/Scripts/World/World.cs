﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class World
{
    private Tile[,] tiles;

    public int width  { private set; get; }
    public int height { private set; get; }

    public World ( int width, int height )
    {
        this.width = width;
        this.height = height;
    }

    public void SetTiles ( Tile[,] tiles ) => this.tiles = tiles;

    public Tile GetTileAt ( int x, int y )
    {
        if ( x >= width || x < 0 || y >= height || y < 0 )
            return null;

        return tiles[x, y];
    }
}
