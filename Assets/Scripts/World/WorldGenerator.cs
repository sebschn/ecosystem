﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public static class WorldGenerator
{
    private static World world;
    private static Tile[,] tiles;

    private static WorldConfig config;
    private static List<Terrain> terrains;

    public static World GetWorld ( WorldConfig config )
    {
        WorldGenerator.config = config;

        world = new World( config.width, config.height );

        GenerateWorld();

        return world;
    }

    private static void GenerateWorld ()
    {
        tiles = new Tile[world.width, world.height];

        for ( int x = 0; x < world.width; x++ )
        {
            for ( int y = 0; y < world.height; y++ )
            {
                CreateTile( x, y );
            }
        }

        world.SetTiles( tiles );
    }

    private static void CreateTile ( int x, int y )
    {
        Tile newTile = new Tile( world, x, y, 0 );

        /*
         * Set the tile height
        */
        SetHeightPerlin( newTile );

        /*
         * Set the terrain type based on the height
        */
        terrains = config.terrains.OrderBy( t => t.heightThreshold ).ToList();
        SetTerrainType( newTile );

        /*
         * Tile settings
        */
        GenerateVegetation( newTile );

        tiles[x, y] = newTile;
    }

    private static void SetHeightPerlin ( Tile tile )
    {
        float perlin = config.minHeight;

        foreach ( HeightModifier modifier in config.heightModifiers )
        {
            perlin += Mathf.PerlinNoise( ( tile.x + modifier.offset.x ) * modifier.scale, ( tile.y + modifier.offset.y ) * modifier.scale ) * modifier.strength;
            perlin = Mathf.Clamp( perlin, 0, config.maxHeight );
        }

        tile.SetHeight( perlin );
    }

    private static void SetTerrainType ( Tile tile )
    {
        Terrain.Type type = Terrain.Type.None;

        foreach ( Terrain terrain in terrains )
        {
            if ( terrain.type == Terrain.Type.Water )
            {
                if ( tile.height <= terrain.heightThreshold )
                {
                    type = Terrain.Type.Water;
                    break;
                }
            }

            if ( tile.height >= terrain.heightThreshold )
                type = terrain.type;
        }

        if ( type == Terrain.Type.None )
            type = terrains[0].type;

        tile.Type = type;
    }

    private static void GenerateVegetation( Tile tile )
    {
        float randomVal = Random.value;
        if ( randomVal <= config.treeProbability )
        {
            tile.SetEntity( new Plant( Species.Tree, Plant.Type.Tree, tile, $"Tree { tile.x }_{ tile.y }" ) );
            return;
        }

        randomVal = Random.value;
        if ( randomVal <= config.plantProbability )
        {
            tile.SetEntity( new Plant( Species.Plant, Plant.Type.Bush, tile, $"Bush { tile.x }_{ tile.y }" ) );
            return;
        }
    }
}
