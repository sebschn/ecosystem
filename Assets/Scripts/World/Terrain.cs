﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Terrain
{
    public enum Type
    {
        None,
        Grass,
        Dirt,
        Sand,
        Snow,
        Stone,
        Water
    }

    public string name;

    public Type type;

    /// <summary>
    /// Terrain will only go up to this height
    /// </summary>
    public float heightThreshold = 0f;

    [Header( "Terrain Material" )]
    public Material material;
}
