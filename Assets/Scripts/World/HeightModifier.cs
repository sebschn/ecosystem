﻿using UnityEngine;

[System.Serializable]
public class HeightModifier
{
    public Vector2 offset = Vector2.zero;

    [Range( 0f, 1f )]
    public float scale = 0.1f;

    [Range( 0f, 10f )]
    public float strength = 1f;
}
