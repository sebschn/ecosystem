﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tile
{
    private Terrain.Type type;
    public Terrain.Type Type
    {
        get { return type; }
        set
        {
            if ( type == value )
                return;

            type = value;
            onTileTypeChanged?.Invoke( this, this );
        }
    }

    private World world { set; get; }
    public int x { private set; get; }
    public int y { private set; get; }

    public float height { private set; get; }


    // WorldObject, WorldEntity
    // Object on this tile - Tree, plant
    public LivingEntity entity { private set; get; }


    public EventHandler<Tile> onTileTypeChanged;
    public EventHandler<Tile> onHeightChanged;

    public Tile ( World world, int x, int y, float height )
    {
        this.world = world;
        this.x = x;
        this.y = y;
        this.height = height;
    }

    public void SetEntity( LivingEntity entity )
    {
        this.entity = entity;
    }

    public void RemoveEntity ()
    {
        entity = null;
    }

    public void SetHeight ( float height )
    {
        this.height = height;
        onHeightChanged?.Invoke( this, this );
    }
}
