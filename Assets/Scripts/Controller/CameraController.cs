﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    [Header( "Position" )]
    [SerializeField]
    float mainSpeed = 25f; // Regular Mouse Speed
    [SerializeField]
    float startingHeight = 20f;

    [Header( "Rotation" )]
    [SerializeField]
    float camSens = 0.15f; // MouseSensitivity

    private Vector3 lastMouse = new Vector3( 255, 255, 255 );
    private float totalRun = 1.0f;

    private void Awake ()
    {
        //Cursor.lockState = CursorLockMode.Confined;
    }

    private void Start ()
    {
        transform.position = new Vector3( WorldManager.Instance.WorldConfig.width / 2, startingHeight, 0 );
    }

    void Update ()
    {
        HandleMouseRotation();
        HandleMouseMovement();
    }

    private void HandleMouseRotation ()
    {
        if ( Input.GetMouseButtonDown( 1 ) )
        {
            lastMouse = Input.mousePosition;
        }

        if ( Input.GetMouseButton( 1 ) )
        {
            lastMouse = Input.mousePosition - lastMouse;
            lastMouse = new Vector3( -lastMouse.y * camSens, lastMouse.x * camSens, 0 );
            lastMouse = new Vector3( transform.eulerAngles.x + lastMouse.x, transform.eulerAngles.y + lastMouse.y, 0 );
            transform.eulerAngles = lastMouse;
            lastMouse = Input.mousePosition;
        }
    }

    private void HandleMouseMovement()
    {
        float f = 0.0f;
        Vector3 p = GetBaseInput();

        totalRun = Mathf.Clamp( totalRun * 0.5f, 1f, 1000f );
        p = p * mainSpeed;


        p = p * Time.deltaTime;
        Vector3 newPosition = transform.position;
        if ( Input.GetKey( KeyCode.Space ) )
        { //If player wants to move on X and Z axis only
            transform.Translate( p );
            newPosition.x = transform.position.x;
            newPosition.z = transform.position.z;
            transform.position = newPosition;
        }
        else
        {
            transform.Translate( p );
        }
    }

    private Vector3 GetBaseInput ()
    { 
        Vector3 p_Velocity = new Vector3();

        if ( Input.GetKey( KeyCode.W ) )
        {
            p_Velocity += new Vector3( 0, 0, 1 );
        }
        if ( Input.GetKey( KeyCode.S ) )
        {
            p_Velocity += new Vector3( 0, 0, -1 );
        }
        if ( Input.GetKey( KeyCode.A ) )
        {
            p_Velocity += new Vector3( -1, 0, 0 );
        }
        if ( Input.GetKey( KeyCode.D ) )
        {
            p_Velocity += new Vector3( 1, 0, 0 );
        }

        return p_Velocity;
    }
}
