﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Plant : LivingEntity
{
    public enum Type
    {
        Tree,
        Bush,
        Grass
    }

    public Type type { private set; get; }

    
    public Plant ( Species species, Type type, Tile tile, string name = "Plant" ) : base ( species, tile, name )
    {
        this.type = type;
    }
}
