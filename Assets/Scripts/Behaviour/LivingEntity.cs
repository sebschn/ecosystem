﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public enum CauseOfDeath
{
    Hunger,
    Thirst,
    Eaten,
    Age
}

public enum Species
{
    None, 
    Tree,
    Plant
}

public class LivingEntity
{
    /*
     * Base Entity Settings
    */
    public string name { private set; get; }
    public Species species { private set; get; }
    public float lifespan { private set; get; } = 0f;
    public float age { private set; get; }

    private bool isDead = false;
    public EventHandler<LivingEntity> onDeath;

    public Tile tile { private set; get; }

    public LivingEntity( Species species, Tile tile, string name )
    {
        this.species = species;
        this.tile = tile;
        this.name = name;
        this.age = 0f;
    }

    public void SetLifespan ( float value ) => lifespan = value > 0 ? value : 0;

    protected virtual void Die ( CauseOfDeath cause )
    {
        if ( isDead )
            return;

        if ( tile.entity == this )
            tile.RemoveEntity();

        isDead = true;
        onDeath?.Invoke( this, this );
    }
}
