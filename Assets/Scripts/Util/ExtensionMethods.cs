﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class ExtensionMethods
{
    public static float Map ( this float value, float inputFrom, float inputTo, float outputFrom, float output2 )
    {
        return ( value - inputFrom ) / ( inputTo - inputFrom ) * ( output2 - outputFrom ) + outputFrom;
    }

    public static int Map ( this int value, int inputFrom, int inputTo, int outputFrom, int output2 )
    {
        return ( value - inputFrom ) / ( inputTo - inputFrom ) * ( output2 - outputFrom ) + outputFrom;
    }
}
