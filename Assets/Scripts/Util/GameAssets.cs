﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameAssets : MonoBehaviour
{
    private static GameAssets instance;
    public static GameAssets Instance
    {
        get
        {
            if ( instance == null )
                instance = ( Instantiate( Resources.Load( "Game Assets" ) ) as GameObject ).GetComponent<GameAssets>();

            return instance;
        }
    }

    [Header( "Tile Prefab" )]
    public GameObject tilePrefab;

    [Header( "Tile Materials" )]
    public List<Material> tileMaterials;

    [Header( "Terrain Hues" )]
    public List<TerrainHue> terrainHues;

    [Header( "Plants" )]
    [SerializeField]
    private GameObjectOfTerrainType[] trees;
    private Dictionary<Terrain.Type, List<GameObject>> treeDict;

    [SerializeField]
    private GameObjectOfTerrainType[] plants;
    private Dictionary<Terrain.Type, List<GameObject>> plantDict;

    public GameObject[] meats;

    private void Awake ()
    {
        treeDict = new Dictionary<Terrain.Type, List<GameObject>>();
        plantDict = new Dictionary<Terrain.Type, List<GameObject>>();

        InitialiseDictionary( trees, treeDict );
        InitialiseDictionary( plants, plantDict );
    }

    private void InitialiseDictionary( GameObjectOfTerrainType[] array, Dictionary<Terrain.Type, List<GameObject>> dict )
    {
        for ( int i = 0; i < array.Length; i++ )
        {
            if ( !dict.ContainsKey( array[i].terrain ) )
            {
                dict.Add( array[i].terrain, new List<GameObject>() { array[i].obj } );
            }
            else
            {
                dict[array[i].terrain].Add( array[i].obj );
            }
        }
    }

    public GameObject GetTreeOfType ( Terrain.Type terrain ) => GetRandomDictionaryEntryOfType( treeDict, terrain );
    public GameObject GetPlantOfType ( Terrain.Type terrain ) => GetRandomDictionaryEntryOfType( plantDict, terrain );

    private GameObject GetRandomDictionaryEntryOfType( Dictionary<Terrain.Type, List<GameObject>> dict, Terrain.Type type )
    {
        List<GameObject> tempList;
        dict.TryGetValue( type, out tempList );

        if ( tempList == null )
            return null;

        return tempList[Random.Range( 0, tempList.Count )];
    }

    [System.Serializable]
    public struct TerrainHue
    {
        public string name;
        public Terrain.Type type;
        public List<Color> hues;
    }

    [System.Serializable]
    public struct GameObjectOfTerrainType
    {
        public Terrain.Type terrain;
        public GameObject obj;
    }
}
